#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/shm.h> 
#include <errno.h>  
#include <fcntl.h> 
#include <sys/types.h>
#include <string.h>

void myfunc(char *p,sem_t *sem,char **argv,int i){
	
	
 
   	
		sem_wait(sem);
		*p = i % (strlen(*argv)+i);		
        	printf("%s\n",argv[*p+1]);	
		sem_post(sem);
		
		usleep(i);
	
		
}



int main(int argc,char **argv)
{
	int pid;
 	int i=0;
	int j=0;
	char *p;
	sem_t *sem;
	int shmid;
	key_t key;
	unsigned int value=1;
	
	

	key = ftok("ask2_part6.c",1);//Generate a key for the file
	if(key == -1){
		perror("failed ftok\n");
	}
	
	shmid = shmget(key,sizeof(int),0600 | IPC_CREAT);
	
	if(shmid == -1){
		perror("failed shmget\n");
	}

	p = (char *) shmat (shmid, NULL, 0);
	*p='\0';

	int num_of_procs=0;//= the children number

	while(argv[i+1]!=NULL){
		num_of_procs += 1;
		++i;
	}
	
		
	sem = sem_open ("sema",O_CREAT | O_EXCL,0600,value);
	
	int c=0;
	for(i=0;i<num_of_procs;i++){
		pid=fork();
		j=0;
		
		if( pid < 0 ){
			
			sem_unlink("sema");			
			sem_close(sem);			
			perror("fork failded");
			exit(1);
		}		
		else if(pid==0){
			myfunc(p,sem,argv,i);
			
			exit(3);	
				
				
		}				
		
}		
	int status;
		for(i=0;i<num_of_procs;i++){
									
			pid = wait(&status);
			if (WIFEXITED (status)){
				WEXITSTATUS (status);

			}
			else
				printf ("the child process exited abnormally\n");	
			
			shmdt (p);
			shmctl(shmid,IPC_RMID,NULL);
			
		
			sem_unlink ("sema"); 	
			sem_close(sem);	
		}
			
  	return 0;
}

