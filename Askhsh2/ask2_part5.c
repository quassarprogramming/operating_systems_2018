#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/shm.h> 
#include <errno.h>  
#include <fcntl.h> 
#include <sys/types.h>

#define Children 5

sem_t s1,s2;


int main()
{
	char* commd[5] = { "ls -l","date","pwd","ls","ps -l" };
	int pid;
	
	sem_init(&s1,0,1);
	sem_init(&s2,0,1);
	
	for(int i=0;i<Children;i++){
		pid= fork();
		if( pid < 0 ){
			fprintf(stderr,"Fork Failed\n");
			exit(1);
		}		
		else if(pid==0){
			if (i==0 || i==1){
			sem_wait(&s1);
			system(commd[i]);
			sem_post(&s1);
		

			}
			else if(i==2 || i==3){
				sleep(1);
				sem_wait(&s2);
				system(commd[i]);
				sem_post(&s2);
				
			}
			else if(i==4) {
				sleep(1);
				sem_wait(&s1);
				sem_wait(&s2);
				system(commd[i]);
				
			}
			exit(0);
			
			
		}
	}
	int status;
	for(int i=0;i< Children;i++){						
		pid = waitpid(-1,&status,0);
		if (WIFEXITED (status)){
			WEXITSTATUS (status);
		}
	}
	sem_destroy(&s1);
	sem_destroy(&s2);
	return 0;
}

