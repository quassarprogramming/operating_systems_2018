#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>

void nothing(){
	int x=0;

	x+=1;
}

int main(){

	time_t start,end;
	int num_of_procs=6000;
	int i=0;
	int pid;
	start = time(&start);
	
	printf("Arx time deut %li\n,",start);
	while(i<num_of_procs){
		pid=fork();
		
		
		if( pid < 0 ){
			
			perror("fork failded");
			exit(1);
		}		
		else if(pid==0){
			
			nothing();
				
			exit(3);	
				
				
		}
		i++;				
		
	}		
	int status;
	for(i=0;i<num_of_procs;i++){					
		pid = waitpid(-1,&status,0);
		if (WIFEXITED (status)){
			WEXITSTATUS (status);
		}
	}
		
	end = time(&end);
	printf("End time deut %li\n,",end);
	printf("Finished in about %f seconds. \n", difftime(end, start)/num_of_procs);
	return 0;
}

