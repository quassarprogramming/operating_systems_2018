#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
  
int main(){

	int num_of_child=5;
	int j;
	int i;
	int pid;
	for(i=0;i<num_of_child;i++){
		pid=fork();
		j=0;
		
		if( pid < 0 ){
			
			perror("fork failded");
			exit(1);
		}		
		else if(pid>0){
			printf("pid=%i child=%i parent=%i \n",pid,getpid(),getppid());
			sleep(1);
			exit(3);			
		}				
		
	}				
	int status;
	for(i=0;i<num_of_child;i++){							
		pid = wait(&status);
		if (WIFEXITED (status)){
			WEXITSTATUS (status);
		}
	}
	return 0;
}

