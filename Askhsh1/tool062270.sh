#!/bin/bash
# Καϊσούδης Φώτης ΑΜ 6062
# Αδαμόπουλος Κωνσταντίνος AM 6270




if [ "$#" = "0" ]; # Έλεγχος ορισμάτων αν είναι 0 εκτύπωσε το ΑΜ
then 
	echo "6062-6270";
elif [ "$#" = "2" ]; # Έλεγχος ορισμάτων αν είναι 2 εκτύπωσε το αρχείο
then  		
 		args=("$@");
		FILE=*"$2"
		
		if [ $1 = "-f" ];
		then
			awk '$1 ~ /^[^#]/' $FILE # Ektypwnei agnowntas tis grammes pou periexoun to sumbolo #
		fi
elif [ "$#" = "3" ];
then
	args=("$@");
	FILE1=*"$2"
	FILE2=*"$3"
	if [[ $2 = $FILE1 && ${args[0]} = "-f" ]];
	then     
		
               if [ ${args[2]} = "--firstnames" ] ; # Έλεγχος αν το όρισμα είναι firstnames
               then
			awk -F'|' '!/^($|#)/{arr[$3]++} END {for (i in arr) print i}' $FILE1 | sort -n | uniq   #Εκτύπωση των πρώτων δρικριτών ονομάτων κατά αλφαβητική σειρά			 
                elif [ ${args[2]} = "--lastnames" ] ; # Έλεγχος αν το όρισμα είναι lastnames
                then
                        awk -F'|' '!/^($|#)/{arr[$2]++}END {for (i in arr) print i }' $FILE1 | sort | uniq # Εκτύπωση των επωνύμων κατά αλφαβητική σειρά
                elif [  ${args[2]} = "--socialmedia" ]; # Έλεγχος αν το όρισμα είναι browsers
                then
		 	
		awk -F '|' '!/^($|#)/ {sub(/[ \t\r\n]+$/, "", $9); arr[$9]++} END { for (i in arr){printf("%s %d\n", i, arr[i])}}' $FILE1 | sort		
			
		fi
	fi
	if [[ $3 = $FILE2 && ${args[1]} = "-f" ]]
	then
  		if [ ${args[0]} = "--firstnames" ]
		then
			awk -F'|' '!/^($|#)/{arr[$3]++} END {for (i in arr) print i}' $FILE2 | sort -n | uniq

		elif [ ${args[0]} = "--lastnames" ]
		then	
			awk -F'|' '!/^($|#)/{arr[$2]++} END {for (i in arr) print i }' $FILE2 | sort | uniq

		elif [ ${args[0]} = "--socialmedia" ]
		then
		awk -F '|' '!/^($|#)/ {sub(/[ \t\r\n]+$/, "", $9); arr[$9]++} END { for (i in arr){printf("%s %d\n", i, arr[i])}}' $FILE2 | sort
		
		fi
	fi
elif [ "$#" = "4" ];
then	
	FILE1=*"$2"
	FILE2=*"$4"
	
	
	if [[ $2 = $FILE1 && $1 = "-f" ]];
	then
		
		if [ $3 = "--born-since" ]; # Ελεγχος αν υπαρχει το συγκεκριμενο ορισμα 
                then
                	for j in $@
                        do
         	               if [ "${#j}" = "10" ]; # Αν υπαρχει ορισμα με 10 χαρακτηρες ειναι ημερομηνια
                               then
              	          	     bdate="$j";
                               fi
                        done
		

			bndate=$(date -d "$bdate" "+%Y-%m-%d") # Θετω την ημερομηνια σε αυτην την μορφη
	
       			out=$(awk -F'|' -v std="$bndate" '!/^($|#)/{arr[$5]} END {for (i in arr) if (i >= std) print i}' $FILE1) # Ελεγχος και αποθηκευση των ημερομηνιων απο την συγκεκριμενη ημερομηνια και μετα 

        		for cnt in $out
        		do
                		grep  "$cnt" $FILE1 # Εκτυπωση των συγκεκριμενων  ημερομηνιων
		
       			done
                elif [ $3 = "--born-until" ]; # Ομοια με πριν με την μονη διαφορα οτι εκτυπωνει μεχρι μια συγκεκριμενη ημερομηνια
                then
         	        for j in $@
                        do
             	        	if [ "${#j}" = "10" ];
                                then
                           		bdate="$j";
                          	 fi
                        done
	
	
			dt=$(date -d "$bdate" "+%Y-%m-%d")

       			out=$(awk -F'|' -v d="$dt" '!/^($|#)/{arr[$5]} END {for (k in arr) if ( k<=d) print k;exit}' $FILE1)

        		for cnt in $out
        		do
                 		grep $cnt  $FILE1
			done
		fi
		
	
	
		if [ $3 = "-id" ]
		then
			for i in $@ 
			do	
				if echo  $i | egrep -q '^[0-9]+$';  # Αν υπαρχει αριθμος (id)  τοτε εκτυπωνει τα συγκρiμενα πεδια που αντιστιχουν σε αυτον τον (id)
				then 	
					grep -nwF "$i" $FILE1 | head -1 | awk -F'|' -v var="$i" 'p=index($0,var){print $3" "$2" "$5;exit}'
				fi
			done
		fi	
	fi
	if [[ $4 = $FILE2 && $3 = "-f" ]]
	then

		if [ $1 = "--born-since" ]; # Ελεγχος αν υπαρχει το συγκεκριμενο ορισμα 
                then
                	for j in $@
                        do
         	               if [ "${#j}" = "10" ]; # Αν υπαρχει ορισμα με 10 χαρακτηρες ειναι ημερομηνια
                               then
              	          	     bdate="$j";
                               fi
                        done
		

			bndate=$(date -d "$bdate" "+%Y-%m-%d") # Θετω την ημερομηνια σε αυτην την μορφη
	
       			out=$(awk -F'|' -v std="$bndate" '!/^($|#)/{arr[$5]} END {for (i in arr) if (i >= std) print i}' $FILE2) # Ελεγχος και αποθηκευση των ημερομηνιων απο την συγκεκριμενη ημερομηνια και μετα 

        		for cnt in $out
        		do
                		grep  "$cnt" $FILE2 # Εκτυπωση των συγκεκριμενων  ημερομηνιων
		
       			done
                elif [ $1 = "--born-until" ]; # Ομοια με πριν με την μονη διαφορα οτι εκτυπωνει μεχρι μια συγκεκριμενη ημερομηνια
                then
         	        for j in $@
                        do
             	        	if [ "${#j}" = "10" ];
                                then
                           		bdate="$j";
                          	 fi
                        done
	
	
			dt=$(date -d "$bdate" "+%Y-%m-%d")

       			out=$(awk -F'|' -v d="$dt"  '!/^($|#)/{arr[$5]} END {for (k in arr) if( k<=d ) print k;exit}' $FILE2) 

        		for cnt in $out
        		do
                 		grep "$cnt" $FILE2
	      		done
		fi




		if [ $1 = "-id" ]
		then
			for i in $@ 
			do	
				if echo  $i | egrep -q '^[0-9]+$';  # Αν υπαρχει αριθμος (id)  τοτε εκτυπωνει τα συγκρiμενα πεδια που αντιστιχουν σε αυτον τον (id)
					then 	
					grep -nwF "$i" $FILE2 | head -1 | awk -F'|' -v var="$i" 'p=index($0,var){print $3" "$2" "$5;exit}'
				fi
			done
		fi
	fi




elif [ "$#" = "6" ];
then
	FILE1=*"$2" #File position
	FILE2=*"$4"
	FILE3=*"$6"

	arr[0]=0;
        j=0

	if [[ $2 = $FILE1 && $1 = "-f" ]]
	then
		if [ $3 != "--edit" ]
		then

        	for i in $@
        	do
               		if [ "${#i}" = "10" ]; # Αν υπαρχει ορισμα με 10 χαρακτηρες ειναι ημερομηνια
                	then
                        	arr[j++]=$i;
                	fi
        	done
		date1=$(date -d ${arr[0]} +"%Y-%m-%d")
		date2=$(date -d ${arr[1]} +"%Y-%m-%d")

		if [[ "$date1" < "$date2" ]]; # Ελεγχος ποια ημερομηνια ειναι μεγαλυτερη και εκτύπωση των ενδιαμεσων ημερομηνιων
		then
			startdate="$date1"
			enddate="$date2"
			out=$(awk -F'|' -v std="$startdate" -v end="$enddate"  '{arr[$5]} END {for (i in arr) if ( i >= std && i <= end ) print i }' $FILE1)

			for j in $out
		    	do 
				grep "$j" $FILE1
		   	done
		elif [[ "$date1" > "$date2" ]]; # Ομοια με πριν με την διαφορά αν οι ημερομηνιες δοθούν ανάποδα
	        then
	                startdate="$date2"
	                enddate="$date1"
	                out=$(awk -F'|' -v std="$startdate" -v end="$enddate"  '{arr[$5]} END {for (i in arr) if ( i >= std && i <= end ) print i }' $FILE1)

	                for j in $out
	                do
	                        grep "$j" $FILE1
	                done
		else # Ομοια με πριν με την διαφορα οτι αν οι ημερομηνιες ταυτιζονται 
	      	        date="$date1"
        	        out=$(awk -F'|' -v var="$date"  '{arr[$5]} END {for (i in arr) if ( i == var ) print i}' $FILE1)
        	        for j in $out
        	        do
        	                grep "$j" $FILE1
        	        done
		fi
	
	fi
	fi
	if [[ $4 = $FILE2 && $3 = "-f" ]];
	then
		for i in $@
       	 	do
               		if [ "${#i}" = "10" ]; # Αν υπαρχει ορισμα με 10 χαρακτηρες ειναι ημερομηνια
                	then
                        	arr[j++]=$i;
                	fi
        	done
		date1=$(date -d ${arr[0]} +"%Y-%m-%d")
		date2=$(date -d ${arr[1]} +"%Y-%m-%d")
		if [[ "$date1" < "$date2" ]]; # Ελεγχος ποια ημερομηνια ειναι μεγαλυτερη και εκτύπωση των ενδιαμεσων ημερομηνιων
		then
			startdate="$date1"
			enddate="$date2"
			out=$(awk -F'|' -v std="$startdate" -v end="$enddate"  '{arr[$5]} END {for (i in arr) if ( i >= std && i <= end ) print i }' $FILE2)

			for j in $out
		    	do 
				grep "$j" $FILE2
		   	done
		elif [[ "$date1" > "$date2" ]]; # Ομοια με πριν με την διαφορά αν οι ημερομηνιες δοθούν ανάποδα
	        then
	                startdate="$date2"
	                enddate="$date1"
	                out=$(awk -F'|' -v std="$startdate" -v end="$enddate"  '{arr[$5]} END {for (i in arr) if ( i >= std && i <= end ) print i }' $FILE2)

	                for j in $out
	                do
	                        grep "$j" $FILE2
	                done
		else # Ομοια με πριν με την διαφορα οτι αν οι ημερομηνιες ταυτιζονται 
	      	        date="$date1"
	                out=$(awk -F'|' -v var="$date"  '{arr[$5]} END {for (i in arr) if ( i == var ) print i}' $FILE2)
	                for j in $out
	                do
	                        grep "$j" $FILE2
	                done
		fi


	fi

	if [[ $6 = $FILE3 && $5 = "-f" ]];
	then
		if [ $1 != "--edit" ]
		then
		for i in $@
	        do
	               if [ "${#i}" = "10" ]; # Αν υπαρχει ορισμα με 10 χαρακτηρες ειναι ημερομηνια
	                then
	                        arr[j++]=$i;
	                fi
	        done
		date1=$(date -d ${arr[0]} +"%Y-%m-%d")
		date2=$(date -d ${arr[1]} +"%Y-%m-%d")
		if [[ "$date1" < "$date2" ]]; # Ελεγχος ποια ημερομηνια ειναι μεγαλυτερη και εκτύπωση των ενδιαμεσων ημερομηνιων
		then
			startdate="$date1"
			enddate="$date2"
			out=$(awk -F'|' -v std="$startdate" -v end="$enddate"  '{arr[$5]} END {for (i in arr) if ( i >= std && i <= end ) print i }' $FILE3)

			for j in $out
		    	do 
				grep "$j" $FILE3
		   	done
		elif [[ "$date1" > "$date2" ]]; # Ομοια με πριν με την διαφορά αν οι ημερομηνιες δοθούν ανάποδα
	        then
	                startdate="$date2"
	                enddate="$date1"
	                out=$(awk -F'|' -v std="$startdate" -v end="$enddate"  '{arr[$5]} END {for (i in arr) if ( i >= std && i <= end ) print i }' $FILE3)

	                for j in $out
	                do
	                        grep "$j" $FILE3
        	        done
		else # Ομοια με πριν με την διαφορα οτι αν οι ημερομηνιες ταυτιζονται 
	      	        date="$date1"
	                out=$(awk -F'|' -v var="$date"  '{arr[$5]} END {for (i in arr) if ( i == var ) print i}' $FILE3)
	                for j in $out
	                do
	                        grep "$j" $FILE3
	                done
		fi	
	fi
	fi

	if [[ $3 = "--edit" ]] && [[ $2 = $FILE1 && $1 = "-f" ]] && [[ $5 >=2 && $5 <=9 ]] #Elegxos tis 8eseis twn orismatwn
	then
		if echo  $4 | egrep -q '^[0-9]+$';		
		then
			
			
			# Apomonosh thn timh ths sthlhs pou 8eloume gia antikatastash
			v=$(awk -F'|' -v var="$4"  '{arr[$1]} END {for (i in arr)  if ( i == var ) print i}' $FILE1 )

			out1=$(grep -Fwn "$v" $FILE1 | awk -F'|' -v var1="$v" -v cols1="$5" 'p=index($0,var1){print $cols1;exit}') 

			sed -z '/'$v'|.*|[^|]*$/s/'$out1'/'$6'/' $FILE1 > output.txt 
			
			cat output.txt > $FILE1 #Ektupwsei
			
			rm -rf output.txt

		fi
	fi
	
									#column								
	if [[ $1 = "--edit" ]] && [[ $6 = $FILE1 && $5 = "-f" ]] && [[ $3 >=2 && $3 <=9 ]] #Elegxos tis 8eseis twn orismatwn
	then
		if echo  $2 | egrep -q '^[0-9]+$';		
		then
			
			# Apomonosh thn timh ths sthlhs pou 8eloume gia antikatastash
			v=$(awk -F'|' -v var="$2"  '{arr[$1]} END {for (i in arr)  if ( i == var ) print i}' $FILE1 )

			out1=$(grep -Fwn "$v" $FILE3 | awk -F'|' -v var1="$v" -v cols1="$3" 'p=index($0,var1){print $cols1;exit}') 

			sed -z '/'$v'|.*|[^|]*$/s/'$out1'/'$4'/' $FILE3 > output.txt 
			
			cat output.txt > $FILE3 #Ektupwsei
			
			rm -rf output.txt

		fi
	fi

	

else
  echo "no right option"
fi


